const http = require('http');

const hostname = '127.0.0.1';
const port = 3000;

const server = http.createServer((req, res) => {
  const pug = require('pug');
  const pugdoc = pug.compileFile('templates/index.pug')
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/html');
  res.end(pugdoc({
      name: 'Forbes'
  }));
});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});
