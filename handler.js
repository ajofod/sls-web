'use strict';

const querystring = require('querystring');
const pug = require('pug');
const pugdoc = pug.compileFile('templates/index.pug')

module.exports.pugworld = (event, context, callback) => {
    console.log('hello AWS');

    var name='Anon';
    if(event.httpMethod==='GET') {
        var params = event.queryStringParameters;
        console.log(JSON.stringify(params));
        if(params && params.name){ // if NAME is a parameter
            name = params.name; // print it
        }
    }else {
        var log = querystring.parse(event.body);
        console.log(log);
        if (log && log.name) {
            name = log.name;
        }
    }

    const response = {
        statusCode: 200,
        headers: {
            'Access-Control-Allow-Origin': '*', // Required for CORS support to work
            'Content-Type': 'text/html', // Since output is in HTML.
        },
        body: pugdoc({
            name: name,
        }) + '<!--\n' + JSON.stringify(event, undefined, 2) + '\n-->'
    };

    callback(null, response);
}


// User Pool Id : us-east-1_yiFkYYP3x
// Pool ARN arn:aws:cognito-idp:us-east-1:552612656000:userpool/us-east-1_yiFkYYP3x
// app client id: 181k12cvgbgatr63i2oihj2u4d
//
// id pool name : notes-id-pool
// Identity pool ID : us-east-1:0e84ac32-494d-4481-afd0-af041647ad93
// ID Pool ARN: arn:aws:cognito-identity:us-east-1:552612656000:identitypool/us-east-1:0e84ac32-494d-4481-afd0-af041647ad93
//
// S3----------
// bucket name: sls-web-app
//
//
//
// COGNITO IAM resource access policy.
// {
//   "Version": "2012-10-17",
//   "Statement": [
//     {
//       "Effect": "Allow",
//       "Action": [
//         "mobileanalytics:PutEvents",
//         "cognito-sync:*",
//         "cognito-identity:*"
//       ],
//       "Resource": [
//         "*"
//       ]
//     },
//     {
//       "Effect": "Allow",
//       "Action": [
//         "s3:*"
//       ],
//       "Resource": [
//         "arn:aws:s3:::sls-web-app/private/${cognito-identity.amazonaws.com:sub}/*"
//       ]
//     },
//     {
//       "Effect": "Allow",
//       "Action": [
//         "execute-api:Invoke"
//       ],
//       "Resource": [
//         "arn:aws:execute-api:us-east-1:*:x10iblvpg0/*/*/*"
//       ]
//     }
//   ]
// }
//
// npx aws-api-gateway-cli-test \
// --username='admin@example.com' \
// --password='Passw0rd!' \
// --user-pool-id='us-east-1_yiFkYYP3x' \
// --app-client-id='181k12cvgbgatr63i2oihj2u4d' \
// --cognito-region='us-east-1' \
// --identity-pool-id='us-east-1:0e84ac32-494d-4481-afd0-af041647ad93' \
// --invoke-url='https://x10iblvpg0.execute-api.us-east-1.amazonaws.com/dev' \
// --api-gateway-region='us-east-1' \
// --path-template='/notes' \
// --method='POST' \
// --body='{"content":"hello world","attachment":"hello.jpg"}'
